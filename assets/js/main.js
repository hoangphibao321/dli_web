$(function () {
    $('.c-slider1').slick({
      slidesToShow: 1,
      dots: false,
      infinite: true,
      fade: true,
      speed: 1000,
      cssEase: 'linear',
      arrows: false,
      autoplay: 'true',
      autoplaySpeed: 2500,
    });
});

$(function () {
    $('.c-slider2').slick({
        // dots: true,
        // infinite: true,
        // fade: true,
        speed: 1000,
        arrows: false,
        autoplay: 'true',
        autoplaySpeed: 2500,
        slidesToShow: 2,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });
});

$(function() {
	$('.c-card1 blockquote').matchHeight();
});

//menu sp


var hide = 'is-hidden',
  body = $('body');

var scrollPosi;

$(function () {
  $('.c-iconmenu').on('click', function () {
    $(this).toggleClass('is-active');
    $(".c-header__menu").toggleClass('is-open');

    if (body.hasClass(hide)) {
      body.removeClass(hide);
      window.scrollTo(0, scrl_pos);
    } else {
      scrl_pos = $(window).scrollTop();
      body.addClass(hide).css('top', -scrl_pos);
    }
  });
});

$(function () {
  $('.c-header__menu .overlay').on('click', function () {
    $('.c-iconmenu').removeClass('is-active');
    $(".c-header__menu").removeClass('is-open');
    if (body.hasClass(hide)) {
      body.removeClass(hide);
      window.scrollTo(0, scrl_pos);
    }
  });

});

$(function($){
	var elm = $('.c-pagetop1')
	$(window).on('load scroll', function(){
		if ($(window).scrollTop() > 300) {
			$(elm).fadeIn(400);
		} else {
			$(elm).fadeOut(400);
		}
	});
	$(elm).on('click', function() {
		$("html , body").animate({ scrollTop: 0 }, 400);
	});
});	

/* ======================================
scroll header
====================================== */
$(function () {
  $(window).scroll(function(){
    var s_head = $(".c-header")
    var top_hei = $(".l-header").height() + 50;
    var _scrolltop = $(window).scrollTop();

      if( _scrolltop > top_hei){
        s_head.addClass("is-sticky");
        }else{
          s_head.removeClass("is-sticky");
        }
  });
});


/* ======================================
scroll header top
====================================== */

// myDefault JS
(function($) {
  /**
   * Hide top navigation on scroll down
   */
  var pageScroll = function(e) {
      var didScroll;
      var lastScrollTop = 0;
      var delta = 5;
      var navbarHeight = $('.c-header').outerHeight();

      $(window).scroll(function(e) {
          didScroll = true;
      });

      setInterval(function() {
          if (didScroll) {
              hasScrolled();
              didScroll = false;
          }
      });

      function hasScrolled() {
          var st = $(this).scrollTop();
          var navbarHeight = $('.c-header').outerHeight();
          
          // Make sure they scroll more than delta
          if (Math.abs(lastScrollTop - st) <= delta)
              return;

          // If they scrolled down and are past the navbar, add class .main-navigation
          // This is necessary so you never see what is "behind" the navbar
          if (st > lastScrollTop && st > navbarHeight) {
              // Scroll down
              $('.c-header').removeClass('.nav-down').addClass('nav-up');
          } else {
              // Scroll up
              if (st + $(window).height() < $(document).height()) {
                  $('.c-header').removeClass('nav-up').addClass('nav-down');
              }
          }

          lastScrollTop = st;
      }
  }

  // Call the created function
  pageScroll();
})(jQuery);

$(window).on("load resize", function() {
  if ($(".c-header").hasClass("nav-up")) {
      $('.c-header').removeClass('nav-up').addClass('nav-down');
  }
});

$(function ($) {
  if ($(".p-top").length > 0) {
    objectFitImages('.imgBig img');
    // objectFitImages('.imgSmall img');
  }
});


$(function () {
  var url = window.location.href;
  var element = $('.c-navi1 ul li a').filter(function () {
    return this.href == url;
  });
  $(element).parentsUntil('.c-navi1 ul', 'li').addClass('is-active');
});